import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import React from "react";

import Home from "../pages";

it("Should render hello text", () => {
  render(<Home />);
  // screen.debug();
  expect(screen.getByText("Welcome to Nextjs")).toBeInTheDocument();
});
