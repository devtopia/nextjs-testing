import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { getPage, initTestHelpers } from "next-page-tester";

initTestHelpers();

describe("Navigation by Link", () => {
  it("Should route to selected page in navbar", async () => {
    const user = userEvent.setup();
    const { page } = await getPage({
      route: "/index"
    });
    render(page);

    await user.click(screen.getByTestId("blog-nav"));
    expect(await screen.findByText("Blog page")).toBeInTheDocument();
    await user.click(screen.getByTestId("comment-nav"));
    expect(await screen.findByText("Comment page")).toBeInTheDocument();
    await user.click(screen.getByTestId("context-nav"));
    expect(await screen.findByText("Context page")).toBeInTheDocument();
    await user.click(screen.getByTestId("todos-nav"));
    expect(await screen.findByText("Todos page")).toBeInTheDocument();
    await user.click(screen.getByTestId("home-nav"));
    expect(await screen.findByText("Welcome to Nextjs")).toBeInTheDocument();
  });
});
