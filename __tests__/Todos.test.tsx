import "@testing-library/jest-dom/extend-expect";
import { cleanup, render, screen } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { getPage, initTestHelpers } from "next-page-tester";
import React from "react";
import { SWRConfig } from "swr";

import Todos from "../pages/todos";
import { ITodo } from "../types/todo";

initTestHelpers();
const server = setupServer(
  rest.get("https://jsonplaceholder.typicode.com/todos/", (req, res, ctx) => {
    const query = req.url.searchParams;
    const _limit = query.get("_limit");
    if (_limit === "10") {
      return res(
        ctx.status(200),
        ctx.json([
          {
            userId: 1,
            id: 1,
            title: "Task A",
            completed: false
          },
          {
            userId: 2,
            id: 2,
            title: "Task B",
            completed: true
          },
          {
            userId: 3,
            id: 3,
            title: "Static task C",
            completed: true
          },
          {
            userId: 4,
            id: 4,
            title: "Static task D",
            completed: false
          }
        ])
      );
    }
  })
);
beforeAll(() => {
  server.listen();
});
afterEach(() => {
  server.resetHandlers();
  cleanup();
});
afterAll(() => {
  server.close();
});
describe(`Todos page / getStaticProps`, () => {
  it("Should render the list of tasks pre-fetched by getStaticProps", async () => {
    const { page } = await getPage({
      route: "/todos"
    });
    render(page);
    expect(await screen.findByText(/todos page/i)).toBeInTheDocument();
    expect(screen.getByText(/static task c/i)).toBeInTheDocument();
    expect(screen.getByText(/static task d/i)).toBeInTheDocument();
  });
});

describe(`Todos page / useSWR`, () => {
  const staticProps: ITodo[] = [
    {
      userId: 3,
      id: 3,
      title: "Static task C",
      completed: true
    },
    {
      userId: 4,
      id: 4,
      title: "Static task D",
      completed: false
    }
  ];
  it("Should render CSF data after pre-rendered data", async () => {
    render(
      <SWRConfig value={{ dedupingInterval: 0 }}>
        <Todos staticTodos={staticProps} />
      </SWRConfig>
    );
    expect(await screen.findByText(/static task c/i)).toBeInTheDocument();
    expect(screen.getByText(/static task d/i)).toBeInTheDocument();
    screen.debug();
    expect(await screen.findByText(/task a/i)).toBeInTheDocument();
    expect(screen.getByText(/task b/i)).toBeInTheDocument();
    screen.debug();
  });
  it("Should render Error text when fetch failed", async () => {
    server.use(
      rest.get("https://jsonplaceholder.typicode.com/todos/", (req, res, ctx) => {
        const query = req.url.searchParams;
        const _limit = query.get("_limit");
        if (_limit === "10") {
          return res(ctx.status(400));
        }
      })
    );
    render(
      <SWRConfig value={{ dedupingInterval: 0 }}>
        <Todos staticTodos={staticProps} />
      </SWRConfig>
    );
    expect(await screen.findByText("Error!")).toBeInTheDocument();
  });
});
