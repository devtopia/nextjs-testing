import { GetStaticPaths, GetStaticProps } from "next";
import Link from "next/link";
import { ParsedUrlQuery } from "querystring";
import React, { ReactElement } from "react";

import Layout from "../../components/Layout";
import { getAllPostData, getPostData } from "../../lib/fetch";

interface IProps {
  id: string;
  title: string;
  body: string;
}

export interface IParams extends ParsedUrlQuery {
  id: string;
}

const PostDetail = (props: IProps): ReactElement => {
  const { id, title, body } = props;
  return (
    <Layout title={title}>
      <p className="m-4">
        {"ID : "}
        {id}
      </p>
      <p className="mb-4 text-xl font-bold">{title}</p>
      <p className="mx-10 mb-12">{body}</p>
      <Link href="/blog">
        <div className="flex cursor-pointer mt-12">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6 mr-3"
          >
            <path strokeLinecap="round" strokeLinejoin="round" d="M11.25 4.5l7.5 7.5-7.5 7.5m-6-15l7.5 7.5-7.5 7.5" />
          </svg>
          <a data-testid="back-blog">Back to blog</a>
        </div>
      </Link>
    </Layout>
  );
};
export default PostDetail;

export const getStaticPaths: GetStaticPaths = async () => {
  const posts = await getAllPostData();
  const paths = posts.map((post) => `/posts/${post.id}`);
  return {
    paths,
    fallback: false
  };
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { id } = ctx.params as IParams;
  const post = await getPostData(id);
  return {
    props: {
      ...post
    }
  };
};
