import { GetStaticProps } from "next";
import React, { ReactElement } from "react";

import Layout from "../components/Layout";
import Post from "../components/Post";
import { getAllPostData } from "../lib/fetch";
import { IPost } from "../types/post";

interface Props {
  posts: IPost[];
}

const Blog = (props: Props): ReactElement => {
  const { posts } = props;
  return (
    <Layout title="Blog">
      <p className="text=4xl mb-10">Blog page</p>
      <ul>
        {posts?.map((post) => (
          <Post key={post.id} {...post} />
        ))}
      </ul>
    </Layout>
  );
};
export default Blog;

export const getStaticProps: GetStaticProps = async () => {
  const posts = await getAllPostData();
  return {
    props: { posts }
  };
};
