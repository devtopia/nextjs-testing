import axios from "axios";
import React, { ReactElement } from "react";
import useSWR from "swr";

import Comment from "../components/Comment";
import Layout from "../components/Layout";
import { IComment } from "../types/comment";

const axiosFetcher = async (): Promise<IComment[]> => {
  const res = await axios.get<IComment[]>("https://jsonplaceholder.typicode.com/comments?_limit=10");
  return res.data;
};

const Comments = (): ReactElement => {
  const { data: comments, error } = useSWR("commentsFetch", axiosFetcher);

  if (error != null) return <span>Error!</span>;

  return (
    <Layout title="Comment">
      <p className="text=4xl m-10">Comment page</p>
      <ul>
        {comments?.map((comment) => (
          <Comment key={comment.id} {...comment} />
        ))}
      </ul>
    </Layout>
  );
};
export default Comments;
