import axios from "axios";
import { GetStaticProps } from "next";
import React, { ReactElement } from "react";
import useSWR from "swr";

import Layout from "../components/Layout";
import { getAllTodoData } from "../lib/fetch";
import { ITodo } from "../types/todo";

interface IProps {
  staticTodos: ITodo[];
}

const axiosFetcher = async (): Promise<ITodo[]> => {
  const res = await axios.get("https://jsonplaceholder.typicode.com/todos/?_limit=10");
  return res.data;
};

const Todos = (props: IProps): ReactElement => {
  const { staticTodos } = props;
  const { data: tasks, error } = useSWR("todosFetch", axiosFetcher, {
    fallbackData: staticTodos,
    revalidateOnMount: true
  });
  if (error != null) return <span>Error!</span>;
  return (
    <Layout title="Todos">
      <p className="text=4xl">Todos page</p>
      <ul>
        {tasks?.map((task) => (
          <li key={task.id}>
            {task.id}
            {": "}
            <span>{task.title}</span>
          </li>
        ))}
      </ul>
    </Layout>
  );
};
export default Todos;

export const getStaticProps: GetStaticProps = async () => {
  const staticTodos = await getAllTodoData();
  return {
    props: { staticTodos }
  };
};
