import fetch from "node-fetch";

import { IComment } from "../types/comment";
import { IPost } from "../types/post";
import { ITodo } from "../types/todo";

export const getAllPostData = async (): Promise<IPost[]> => {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts/?_limit=10");
  const posts = await res.json();
  return posts;
};

export const getAllTodoData = async (): Promise<ITodo[]> => {
  const res = await fetch("https://jsonplaceholder.typicode.com/todos/?_limit=10");
  const todos = await res.json();
  return todos;
};

export const getPostData = async (id: string): Promise<IPost> => {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  const post = await res.json();
  return post;
};

export const getAllCommentData = async (): Promise<IComment[]> => {
  const res = await fetch("https://jsonplaceholder.typicode.com/comments/?_limit=10");
  const comments = await res.json();
  return comments;
};
