import React, { ReactElement, ReactNode, createContext, useContext, useState } from "react";

interface IStateContext {
  toggle: boolean;
  setToggle: React.Dispatch<React.SetStateAction<boolean>>;
}

interface IProps {
  children: ReactNode;
}

const StateContext = createContext({} as IStateContext);

export const StateProvider = (props: IProps): ReactElement => {
  const { children } = props;
  const [toggle, setToggle] = useState(false);

  return <StateContext.Provider value={{ toggle, setToggle }}>{children}</StateContext.Provider>;
};
export const useStateContext = (): IStateContext => useContext(StateContext);
