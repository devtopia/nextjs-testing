import React, { ReactElement } from "react";

import { IComment } from "../types/comment";

const Comment = ({ id, name, body }: IComment): ReactElement => {
  return (
    <li className="mx-10">
      <p>
        {id}
        {": "}
        {body}
      </p>
      <p className="text-center mt-3 mb-10">
        {"by "}
        {name}
      </p>
    </li>
  );
};

export default Comment;
