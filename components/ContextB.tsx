import React, { ReactElement } from "react";

import { useStateContext } from "../context/StateProvider";

const ContextB = (): ReactElement => {
  const { toggle } = useStateContext();
  return (
    <>
      <p>Context B</p>
      <p className="mb-5 text-indigo-600" data-testid="toggle-b">
        {String(toggle)}
      </p>
    </>
  );
};

export default ContextB;
