import Link from "next/link";
import React, { ReactElement } from "react";

interface Props {
  id: number;
  title: string;
}

const Post = (props: Props): ReactElement => {
  const { id, title } = props;
  return (
    <div>
      <span>{id}</span>
      {" : "}
      <Link href={`/posts/${id}`}>
        <a className="cursor-pointer border-b border-gray-500 hover:bg-gray-300">{title}</a>
      </Link>
    </div>
  );
};
export default Post;
